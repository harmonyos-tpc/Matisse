![Image](image/banner.png)

# Matisse

Matisse is a well-designed local image and video selector for openharmony. You can
- Use it in Ability or AbilitySlice
- Select images including JPEG, PNG and videos including MPEG, MP4
- Apply different themes, including two built-in themes and custom themes
- Different image loaders
- Define custom filter rules
- More to find out yourself
- **Theme style changes as the system changes(not setThemeId())**

GIF is not supported due Glide issues.for details see [Glide](https://gitee.com/openharmony-tpc/glide)

| Zhihu Style                    | Dracula Style                     | Preview                          |
|:------------------------------:|:---------------------------------:|:--------------------------------:|
|![](image/screenshot_zhihu.png) | ![](image/screenshot_dracula.png) | ![](image/screenshot_preview.png)|

## Download
Gradle:

```groovy
repositories {
    mavenCentral()
}

dependencies{
    implementation 'io.openharmony.tpc.thirdlib:Matisse:1.0.6'
}
```

Check out [Matisse releases](https://gitee.com/openharmony-tpc/Matisse/releases) to see more unstable versions.

## ProGuard
If you use [Glide](https://gitee.com/openharmony-tpc/glide) as your image engine, add rules as Glide's README says.
And add extra rule:

```pro
-dontwarn com.squareup.picasso.**
```

If you use [Picasso](https://gitee.com/openharmony-tpc/picasso) as your image engine, add rules as Picasso's README says.
And add extra rule:
```pro
-dontwarn com.bumptech.glide.**
```
**Attention**: The above progurad rules are correct.

## How do I use Matisse?
#### Permission
The library requires three permissions:
- `ohos.permission.READ_MEDIA`
- `ohos.permission.WRITE_MEDIA`
- `ohos.permission.CAMERA`


#### Simple usage snippet
------
Start `MatisseAbility` from current `Ability` or `AbilitySlice`:

```java
Matisse.from(MatisseAbility.this)
        .choose(MimeType.allOf())
        .countable(true)
        .maxSelectable(9)
        .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
        .thumbnailScale(0.85f)
        .imageEngine(new GlideEngine())
        .showPreview(false) // Default is `true`
        .forResult(REQUEST_CODE_CHOOSE);
```


And Also you can define your own theme as you wish.

#### Receive Result
In `onAbilityResult()` callback of the starting `Ability` or `AbilitySlice`:

```java
List<Uri> mSelected;

@Override
protected void onAbilityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
        mSelected = Matisse.obtainResult(data);
        Log.d("Matisse", "mSelected: " + mSelected);
    }
}
```



## Contributing
[Matisse is an Open Source Project](https://gitee.com/openharmony-tpc/Matisse/blob/master/CONTRIBUTING.md)

## Thanks
This library is inspired by [Laevatein] and uses some of its source code.

## License

    Copyright 2017 Zhihu Inc.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
       http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.


