# 1.0.6
1. Fixed video thumbnails
2. Fixed Picasso loading image location

# 1.0.5
1. change the `AbilitySlice` to `Fraction`
2. fix the bug that dialog do not disappear

# 1.0.4
1. fix '#I3XO0C' '#I3YAZV'
2. fix 'autoHideToolbarOnSingleTap'
3. fix 'addFilter'

# 1.0.2
1. fix `#I3TA5P`
2. add `GlideEngine` & `PicassoEngine`
3. change the default loader to `GlideEngine`
4. add night mode, but according to system settings
5. fix query data sorting