/*
 * Copyright (C) 2014 nohana, Inc.
 * Copyright 2017 Zhihu Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zhihu.matisse.model;


import com.zhihu.matisse.MimeType;
import ohos.aafwk.ability.DataUriUtils;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;
import ohos.utils.net.Uri;

import java.util.Objects;

public class Item implements Sequenceable{

    public static final long ITEM_ID_CAPTURE = -1;
    public static final String ITEM_DISPLAY_NAME_CAPTURE = "Capture";
    public long id;
    public String mimeType;
    public Uri uri;
    public long size;
    public String data;
    public long date_add;
    public long duration; // only for video, in ms

    private Item(long id, String mimeType, long size,String data, long duration,long date_add) {
        this.id = id;
        this.mimeType = mimeType;
        Uri contentUri;
        if (isImage()) {
            contentUri = AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI;
        } else if (isVideo()) {
            contentUri = AVStorage.Video.Media.EXTERNAL_DATA_ABILITY_URI;
        } else {
            // ?
            contentUri = AVStorage.Files.fetchResource("external");
        }
        this.uri = DataUriUtils.attachId(contentUri, id);
        this.size = size;
        this.data = data;
        this.duration = duration;
        this.date_add = date_add;
    }

    public static Item valueOf(ResultSet cursor) {
        long duration = cursor.getLong(cursor.getColumnIndexForName(AVStorage.AVBaseColumns.DURATION));
        long date_add = cursor.getLong(cursor.getColumnIndexForName(AVStorage.AVBaseColumns.DATE_ADDED));
        return new Item(cursor.getLong(cursor.getColumnIndexForName(AVStorage.AVBaseColumns.ID)),
                cursor.getString(cursor.getColumnIndexForName(AVStorage.AVBaseColumns.MIME_TYPE)),
                cursor.getLong(cursor.getColumnIndexForName(AVStorage.AVBaseColumns.SIZE)),
                cursor.getString(cursor.getColumnIndexForName(AVStorage.AVBaseColumns.DATA)),
                duration,date_add);
    }


    public Uri getContentUri() {
        return uri;
    }

    public boolean isCapture() {
        return id == ITEM_ID_CAPTURE;
    }

    public boolean isImage() {
        return MimeType.isImage(mimeType);
    }

    public boolean isGif() {
        return MimeType.isGif(mimeType);
    }

    public boolean isVideo() {
        return MimeType.isVideo(mimeType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return id == item.id &&
                size == item.size &&
                duration == item.duration &&
                mimeType.equals(item.mimeType) &&
                uri.equals(item.uri) &&
                data.equals(item.data) &&
                date_add == (item.date_add);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, mimeType, uri, size, data, date_add, duration);
    }

    @Override
    public boolean hasFileDescriptor() {
        return false;
    }

    @Override
    public boolean marshalling(Parcel parcel) {
        if (!parcel.writeLong(id)){
            return false;
        }
        if (!parcel.writeString(mimeType)){
            return false;
        }
        parcel.writeTypedSequenceable(uri);

        if (!parcel.writeLong(size)){
            return false;
        }
        if (!parcel.writeString(data)){
            return false;
        }
        if (!parcel.writeLong(duration)){
            return false;
        }
        return parcel.writeLong(date_add);
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", mimeType='" + mimeType + '\'' +
                ", uri=" + uri +
                ", size=" + size +
                ", data='" + data + '\'' +
                ", duration=" + duration +
                ", date_add=" + date_add +
                '}';
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        id = parcel.readLong();
        mimeType = parcel.readString();
        uri = parcel.createSequenceable();
        size = parcel.readLong();
        data = parcel.readString();
        duration = parcel.readLong();
        date_add = parcel.readLong();
        return true;
    }
    public Item(Parcel parcel){
        id = parcel.readLong();
        mimeType = parcel.readString();
        uri = parcel.createSequenceable();
        size = parcel.readLong();
        data = parcel.readString();
        duration = parcel.readLong();
        date_add = parcel.readLong();
    }

    public static final Producer PRODUCER = new Producer(){
        @Override
        public Object createFromParcel(Parcel parcel) {
            return new Item(parcel);
        }
    };
}
