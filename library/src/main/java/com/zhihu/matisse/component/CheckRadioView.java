package com.zhihu.matisse.component;

import com.zhihu.matisse.ResourceTable;
import com.zhihu.matisse.utils.AttrUtils;
import com.zhihu.matisse.utils.L;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.util.Random;

public class CheckRadioView extends Image {

    private int mSelectedColor;
    private int mUnSelectUdColor;

    public CheckRadioView(Context context) {
        super(context);
        init();
    }

    public CheckRadioView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        try {
            mSelectedColor = getContext().getResourceManager().getElement(ResourceTable.Color_zhihu_item_checkCircle_backgroundColor).getColor();
            mUnSelectUdColor = getContext().getResourceManager().getElement(ResourceTable.Color_zhihu_check_original_radio_disable).getColor();
        } catch (IOException | NotExistException | WrongTypeException e) {
            L.d(e.getMessage());
        }

        setChecked(false);
    }

    public void setChecked(boolean enable) {
        if (enable) {
            PixelMap element =  AttrUtils.getPixelMap(mContext,ResourceTable.Media_ic_preview_radio_on);
            Canvas canvas = new Canvas(new Texture(element));
            canvas.drawColor (mSelectedColor, Canvas.PorterDuffMode.SRC_IN);
            setImageElement(new PixelMapElement(element));

        } else {
            PixelMap element =  AttrUtils.getPixelMap(mContext,ResourceTable.Media_ic_preview_radio_off);
            Canvas canvas = new Canvas(new Texture(element));
            canvas.drawColor (mUnSelectUdColor, Canvas.PorterDuffMode.SRC_IN);
            setImageElement(new PixelMapElement(element));
        }
    }

    public void setColor(Color color) {
        PixelMap pixelMap = getPixelMap();
        Canvas canvas = new Canvas(new Texture(pixelMap));
        canvas.drawColor (color.getValue(), Canvas.PorterDuffMode.SRC_IN);
        setImageElement(new PixelMapElement(pixelMap));
    }
}
