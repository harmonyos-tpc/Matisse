/*
 * Copyright 2017 Zhihu Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zhihu.matisse.component;

import com.zhihu.matisse.ResourceTable;
import com.zhihu.matisse.annotation.NonNull;
import com.zhihu.matisse.model.Album;
import ohos.agp.components.*;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;

public class AlbumsSpinner {

    private static final int MAX_SHOWN_COUNT = 6;
    private BaseItemProvider mAdapter;
    private Text mSelected;
    private Context context;
    private PopupDialog mListPopupWindow;
    private ListContainer listContainer;
    private Component anchorComponent;
    private OnItemSelectedListener mOnItemSelectedListener;

    public AlbumsSpinner(@NonNull Context context ,Component anchorComponent) {
        this.context = context;
        this.anchorComponent = anchorComponent;
    }
    private void newDialog(){
        mListPopupWindow = new PopupDialog(context,anchorComponent,AttrHelper.fp2px(240,context),ComponentContainer.LayoutConfig.MATCH_CONTENT);
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_dialog_popup_list,null,false);
        mListPopupWindow.setCustomComponent(component);
        mListPopupWindow.setHasArrow(true);

        mListPopupWindow.setDialogListener(new BaseDialog.DialogListener() {
            @Override
            public boolean isTouchOutside() {
                mListPopupWindow.hide();
                return true;
            }
        });
        listContainer = (ListContainer) component.findComponentById(ResourceTable.Id_dialog_list);
        if (mAdapter != null){
            listContainer.setItemProvider(mAdapter);
        }
        listContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                AlbumsSpinner.this.onItemSelected(listContainer.getContext(), i);
                if (mOnItemSelectedListener != null) {
                    mOnItemSelectedListener.onItemSelected(listContainer, component, i, l);
                }
            }
        });
    }

    public void setOnItemSelectedListener(OnItemSelectedListener listener) {
        mOnItemSelectedListener = listener;
    }

    public void setSelection(Context context, int position) {
        onItemSelected(context, position);
    }

    private void onItemSelected(Context context, int position) {
        if (mListPopupWindow != null){
            mListPopupWindow.hide();
        }
        Album album = (Album) mAdapter.getItem(position);
        String displayName = album.getDisplayName(context);
        if (mSelected.getVisibility() == Component.VISIBLE) {
            mSelected.setText(displayName);
        } else {
            mSelected.setAlpha(0.0f);
            mSelected.setVisibility(Component.VISIBLE);
            mSelected.setText(displayName);
            mSelected.createAnimatorProperty().alpha(1.0f).setDuration(1500).start();
        }
    }

    public void setAdapter(BaseItemProvider adapter) {
        if (listContainer != null){
            listContainer.setItemProvider(adapter);
        }
        mAdapter = adapter;
    }

    public void setSelectedTextView(Text textView) {
        mSelected = textView;
        mSelected.setVisibility(Component.HIDE);
        mSelected.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                newDialog();
                mListPopupWindow.showOnCertainPosition(TextAlignment.TOP,0,AttrHelper.fp2px(90,context));
            }
        });
    }

    public void setPopupAnchorView(Component view) {
        anchorComponent = view;
    }
    public interface OnItemSelectedListener {

        void onItemSelected(ListContainer listContainer, Component component, int position, long id);

        void onNothingSelected(ListContainer listContainer);
    }
}
