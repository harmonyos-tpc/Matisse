package com.zhihu.matisse.slice;

import com.zhihu.matisse.model.Item;
import com.zhihu.matisse.model.SelectionSpec;
import com.zhihu.matisse.provider.PreviewProvider;
import ohos.aafwk.content.Intent;

import java.util.ArrayList;

import static com.zhihu.matisse.model.SelectedItemCollection.STATE_SELECTION;

public class SelectPreviewAbility extends BasePreviewAbility{

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        if (!SelectionSpec.getInstance().hasInited) {
            terminateAbility(0);
            return;
        }
        ArrayList<Item> selected = getIntent().getSequenceableArrayListParam(STATE_SELECTION);
        if (selected != null){
            if (previewProvider == null){
                previewProvider = new PreviewProvider(getContext(),selected);
                pageSlider.setProvider(previewProvider);
            }else {
                previewProvider.addAll(selected);
            }
            previewProvider.notifyDataChanged();
            if (mSpec.countable) {
                mCheckView.setCheckedNum(1);
            } else {
                mCheckView.setChecked(true);
            }
            mPreviousPos = 0;
            updateSize(selected.get(0));
        }
    }

}
