package com.zhihu.matisse.slice;

import com.zhihu.matisse.model.Item;
import com.zhihu.matisse.model.SelectionSpec;
import com.zhihu.matisse.provider.PreviewProvider;
import ohos.aafwk.content.Intent;

import java.util.ArrayList;

public class AlbumPreviewAbility extends BasePreviewAbility {
    public static final String EXTRA_ALBUM_LIST = "extra_item_list";
    public static final String EXTRA_ALBUM_INDEX = "extra_item_index";

    private boolean mIsAlreadySetPosition;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        if (!SelectionSpec.getInstance().hasInited) {
            terminateAbility(0);
            return;
        }
        ArrayList<Item> list1 = intent.getSequenceableArrayListParam(EXTRA_ALBUM_LIST);
        if (previewProvider == null){
            previewProvider = new PreviewProvider(getContext(),list1);
            pageSlider.setProvider(previewProvider);
        }else {
            previewProvider.addAll(list1);
        }
        previewProvider.notifyDataChanged();

        if (!mIsAlreadySetPosition) {
            //onAlbumMediaLoad is called many times..
            mIsAlreadySetPosition = true;
            int selectedIndex = getIntent().getIntParam("index",0);

            pageSlider.setCurrentPage(selectedIndex);
            mPreviousPos = selectedIndex;

            Item item = list1.get(selectedIndex);
            if (mSpec.countable) {
                int checkedNum = mSelectedCollection.checkedNumOf(item);
                mCheckView.setCheckedNum(checkedNum);
                if (checkedNum > 0) {
                    mCheckView.setEnabled(true);
                } else {
                    mCheckView.setEnabled(!mSelectedCollection.maxSelectableReached());
                }
            } else {
                boolean checked = mSelectedCollection.isSelected(item);
                mCheckView.setChecked(checked);
                if (checked) {
                    mCheckView.setEnabled(true);
                } else {
                    mCheckView.setEnabled(!mSelectedCollection.maxSelectableReached());
                }
            }
            updateSize(item);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

}
