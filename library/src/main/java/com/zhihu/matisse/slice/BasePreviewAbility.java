package com.zhihu.matisse.slice;

import com.zhihu.matisse.MatisseAbility;
import com.zhihu.matisse.ResourceTable;
import com.zhihu.matisse.component.CheckRadioView;
import com.zhihu.matisse.component.CheckView;
import com.zhihu.matisse.model.IncapableCause;
import com.zhihu.matisse.model.Item;
import com.zhihu.matisse.model.SelectedItemCollection;
import com.zhihu.matisse.model.SelectionSpec;
import com.zhihu.matisse.provider.PreviewProvider;
import com.zhihu.matisse.utils.IncapableDialog;
import com.zhihu.matisse.utils.L;
import com.zhihu.matisse.utils.PhotoMetadataUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.animation.Animator;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.utils.PacMap;
import ohos.utils.net.Uri;

import java.io.IOException;
import java.util.ArrayList;

import static com.zhihu.matisse.model.SelectedItemCollection.STATE_COLLECTION_TYPE;
import static com.zhihu.matisse.model.SelectedItemCollection.STATE_SELECTION;

public class BasePreviewAbility extends Ability implements PageSlider.PageChangedListener{
    public static final String EXTRA_DEFAULT_BUNDLE = "extra_default_bundle";
    public static final String EXTRA_RESULT_BUNDLE = "extra_result_bundle";
    public static final String EXTRA_RESULT_APPLY = "extra_result_apply";
    public static final String EXTRA_RESULT_ORIGINAL_ENABLE = "extra_result_original_enable";
    public static final String CHECK_STATE = "checkState";

    protected final SelectedItemCollection mSelectedCollection = new SelectedItemCollection(this);
    protected SelectionSpec mSpec;


    protected Text tvBack;
    protected Text tvApply;
    protected Text mSize;
    protected Text tvOriginal;

    private CheckRadioView cbOriginal;
    protected CheckView mCheckView;
    private DirectionalLayout dlCheck;
    protected PageSlider pageSlider;

    private boolean mIsToolbarHide = false;
    protected int mPreviousPos = -1;
    protected boolean mOriginalEnable;

    protected PreviewProvider previewProvider;

    private int mSelectedColor = 0x1E8AE8;
    private int mUnSelectUdColor = 0x808080;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        if (!SelectionSpec.getInstance().hasInited) {
            terminateAbility(0);
            return;
        }

        this.getWindow().addFlags(WindowManager.LayoutConfig.MARK_ALLOW_EXTEND_LAYOUT);
        this.getWindow().addFlags(WindowManager.LayoutConfig.MARK_FULL_SCREEN);
        this.getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);

        setUIContent(ResourceTable.Layout_ability_preview);
        mSpec = SelectionSpec.getInstance();
        if (mSpec.needOrientationRestriction()) {
            setDisplayOrientation(mSpec.orientation);
        }

        if (intent == null) {
            mSelectedCollection.onCreate(getIntent());
            mOriginalEnable = getIntent().getBooleanParam(EXTRA_RESULT_ORIGINAL_ENABLE, false);
        } else {
//            PacMap pacMap = getIntent().getParcelableParam(EXTRA_DEFAULT_BUNDLE);
            mSelectedCollection.onCreate(intent);
            mOriginalEnable = getIntent().getBooleanParam(EXTRA_RESULT_ORIGINAL_ENABLE,false);
        }

        tvBack = (Text) findComponentById(ResourceTable.Id_textPreview_back);
        tvApply = (Text) findComponentById(ResourceTable.Id_textPreview_apply);
        mSize = (Text) findComponentById(ResourceTable.Id_textPreview_size);
        tvOriginal = (Text) findComponentById(ResourceTable.Id_textPreview_original);
        cbOriginal = (CheckRadioView) findComponentById(ResourceTable.Id_checkPreview_radio);
        mCheckView = (CheckView) findComponentById(ResourceTable.Id_checkPreview);
        StackLayout top = (StackLayout) findComponentById(ResourceTable.Id_stackPreview_top);
        StackLayout bottom = (StackLayout) findComponentById(ResourceTable.Id_stackPreview_bottom);
        mCheckView.setCountable(mSpec.countable);
        dlCheck = (DirectionalLayout) findComponentById(ResourceTable.Id_dlPreview_check);
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_sliderPreview);
        pageSlider.setOrientation(Component.HORIZONTAL);
        pageSlider.addPageChangedListener(this);
        pageSlider.setPageCacheSize(3);
        try {
            mSelectedColor = getContext().getResourceManager().getElement(ResourceTable.Color_zhihu_item_checkCircle_backgroundColor).getColor();
            mUnSelectUdColor = getContext().getResourceManager().getElement(ResourceTable.Color_zhihu_check_original_radio_disable).getColor();
        } catch (IOException | NotExistException | WrongTypeException e) {
            L.d(e.getMessage());
        }


        tvBack.setClickedListener(component -> parseClick(component,0));
        tvApply.setClickedListener(component -> parseClick(component,3));

        mCheckView.setClickedListener(component -> parseClick(component,1));
        dlCheck.setClickedListener(component -> parseClick(component,2));

        previewProvider = new PreviewProvider(this,new ArrayList<>());
        pageSlider.setProvider(previewProvider);
        previewProvider.setOnPrimaryItemSetListener(new PreviewProvider.OnPrimaryItemSetListener() {
            @Override
            public void onPrimaryItemSet(int position, Uri uri) {
                Intent intent = new Intent();
                intent.setUriAndType(uri,"video/*");
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName(VideoAbility.class.getName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
            }

            @Override
            public void onClick() {
                if (mIsToolbarHide){
                    top.createAnimatorProperty()
                            .setCurveType(Animator.CurveType.DECELERATE)
                            .moveByY(top.getEstimatedHeight())
                            .start();
                    bottom.createAnimatorProperty()
                            .setCurveType(Animator.CurveType.DECELERATE)
                            .moveByY(-bottom.getEstimatedHeight())
                            .start();
                }else {
                    top.createAnimatorProperty()
                            .setCurveType(Animator.CurveType.DECELERATE)
                            .moveByY(-top.getEstimatedHeight())
                            .start();
                    bottom.createAnimatorProperty()
                            .setCurveType(Animator.CurveType.DECELERATE)
                            .moveByY(bottom.getEstimatedHeight())
                            .start();
                }
                mIsToolbarHide = !mIsToolbarHide;
            }
        });

        updateApplyButton();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }

    @Override
    public void onSaveAbilityState(PacMap outState) {
        mSelectedCollection.onSaveInstanceState( outState);
        outState.putBooleanValue("checkState", mOriginalEnable);
        super.onSaveAbilityState(outState);
    }

    @Override
    protected void onBackPressed() {
        sendBackResult(false);
        super.onBackPressed();
    }

    private void parseClick(Component component, int index){
        switch (index){
            case 0:
                onBackPressed();
                break;
            case 3:
                sendBackResult(true);
                terminateAbility();
                break;
            case 1:
                Item item = previewProvider.getMediaItem(pageSlider.getCurrentPage());
                if (mSelectedCollection.isSelected(item)) {
                    mSelectedCollection.remove(item);
                    if (mSpec.countable) {
                        mCheckView.setCheckedNum(CheckView.UNCHECKED);
                    } else {
                        mCheckView.setChecked(false);
                    }
                } else {
                    if (assertAddSelection(item)) {
                        mSelectedCollection.add(item);
                        if (mSpec.countable) {
                            mCheckView.setCheckedNum(mSelectedCollection.checkedNumOf(item));
                        } else {
                            mCheckView.setChecked(true);
                        }
                    }
                }
                updateApplyButton();

                if (mSpec.onSelectedListener != null) {
                    mSpec.onSelectedListener.onSelected(
                            mSelectedCollection.asListOfUri(), mSelectedCollection.asListOfString());
                }
                break;

            case 2:
                int count = countOverMaxSize();
                if (count > 0){
                    IncapableDialog incapableDialog = new IncapableDialog(getContext());
                    try {
                        incapableDialog.setMessage(getResourceManager().getElement(ResourceTable.String_error_over_original_count).getString(count,mSpec.originalMaxSize));
                    } catch (IOException | NotExistException | WrongTypeException e) {
                        incapableDialog.setMessage(String.format("%1$d images over %2$d MB. Original will be unchecked",count,mSpec.originalMaxSize));
                    }
                    incapableDialog.setPositiveButton().show();
                }

                mOriginalEnable = !mOriginalEnable;
                cbOriginal.setChecked(mOriginalEnable);
                tvOriginal.setTextColor(new Color(mOriginalEnable ? mSelectedColor : mUnSelectUdColor));

                if (mSpec.onCheckedListener != null){
                    mSpec.onCheckedListener.onCheck(mOriginalEnable);
                }
                break;
            default:
                break;
        }
    }

    private void updateApplyButton() {
        int selectedCount = mSelectedCollection.count();
        if (selectedCount == 0) {
            try {
                tvApply.setText(getResourceManager().getElement(ResourceTable.String_button_apply_default).getString());
                tvApply.setTextColor(new Color(getResourceManager().getElement(ResourceTable.Color_zhihu_bottom_toolbar_apply_text_disable).getColor()));
            } catch (IOException | NotExistException | WrongTypeException e) {

                tvApply.setText("Apply");
            }

            tvApply.setEnabled(false);
        } else if (selectedCount == 1 && mSpec.singleSelectionModeEnabled()) {
            try {
                tvApply.setText(getResourceManager().getElement(ResourceTable.String_button_apply_default).getString());
                tvApply.setTextColor(new Color(getResourceManager().getElement(ResourceTable.Color_zhihu_bottom_toolbar_apply_text).getColor()));
            } catch (IOException | NotExistException | WrongTypeException e) {
                tvApply.setText("Apply");
            }
            tvApply.setEnabled(true);
        } else {
            tvApply.setEnabled(true);
            try {
                tvApply.setText(getResourceManager().getElement(ResourceTable.String_button_apply).getString(selectedCount));
                tvApply.setTextColor(new Color(getResourceManager().getElement(ResourceTable.Color_zhihu_bottom_toolbar_apply_text).getColor()));
            } catch (IOException | NotExistException | WrongTypeException e) {
                L.d(e.getMessage());
            }
        }

        if (mSpec.originalable) {
            dlCheck.setVisibility(Component.VISIBLE);
            updateOriginalState();
        } else {
            dlCheck.setVisibility(Component.HIDE);
        }
    }


    private void updateOriginalState() {
        cbOriginal.setChecked(mOriginalEnable);
        if (!mOriginalEnable) {
            cbOriginal.setColor(Color.WHITE);
        }

        if (countOverMaxSize() > 0) {

            if (mOriginalEnable) {
                IncapableDialog incapableDialog = new IncapableDialog(getContext());
                try {
                    incapableDialog.setMessage(getResourceManager().getElement(ResourceTable.String_error_over_original_size).getString(mSpec.originalMaxSize));
                } catch (IOException | NotExistException | WrongTypeException e) {
                    incapableDialog.setMessage(String.format("Can\\'t select the images larger than %1$d MB",mSpec.originalMaxSize));
                }
                incapableDialog.setPositiveButton().show();

                cbOriginal.setChecked(false);
                cbOriginal.setColor(Color.WHITE);
                mOriginalEnable = false;
            }
        }
    }


    private int countOverMaxSize() {
        int count = 0;
        int selectedCount = mSelectedCollection.count();
        for (int i = 0; i < selectedCount; i++) {
            Item item = mSelectedCollection.asList().get(i);
            if (item.isImage()) {
                float size = PhotoMetadataUtils.getSizeInMB(item.size);
                if (size > mSpec.originalMaxSize) {
                    count++;
                }
            }
        }
        return count;
    }

    protected void updateSize(Item item) {
        if (item.isGif()) {
            mSize.setVisibility(Component.VISIBLE);
            mSize.setText(PhotoMetadataUtils.getSizeInMB(item.size) + "M");
        } else {
            mSize.setVisibility(Component.HIDE);
        }

        if (item.isVideo()) {
            dlCheck.setVisibility(Component.HIDE);
        } else if (mSpec.originalable) {
            dlCheck.setVisibility(Component.VISIBLE);
        }
    }

    protected void sendBackResult(boolean apply) {
        Intent intent = new Intent();
        intent.setSequenceableArrayListParam(STATE_SELECTION, new ArrayList<>(mSelectedCollection.getItems()));
        intent.setParam(STATE_COLLECTION_TYPE, mSelectedCollection.getCollectionType());
        intent.setParam(EXTRA_RESULT_APPLY, apply);
        intent.setParam(EXTRA_RESULT_ORIGINAL_ENABLE, mOriginalEnable);
        setResult(MatisseAbility.RESULT_OK, intent);
    }

    private boolean assertAddSelection(Item item) {
        IncapableCause cause = mSelectedCollection.isAcceptable(item);
        IncapableCause.handleCause(this, cause);
        return cause == null;
    }

    @Override
    public void onPageSliding(int i, float v, int i1) {

    }

    @Override
    public void onPageSlideStateChanged(int position) {

    }

    @Override
    public void onPageChosen(int position) {
        PreviewProvider adapter = (PreviewProvider) pageSlider.getProvider();
        if (mPreviousPos != -1 && mPreviousPos != position) {
//            ( adapter.createPageInContainer(pageSlider, mPreviousPos)).resetView();

            Item item = adapter.getMediaItem(position);
            if (mSpec.countable) {
                int checkedNum = mSelectedCollection.checkedNumOf(item);
                mCheckView.setCheckedNum(checkedNum);
                if (checkedNum > 0) {
                    mCheckView.setEnabled(true);
                } else {
                    mCheckView.setEnabled(!mSelectedCollection.maxSelectableReached());
                }
            } else {
                boolean checked = mSelectedCollection.isSelected(item);
                mCheckView.setChecked(checked);
                if (checked) {
                    mCheckView.setEnabled(true);
                } else {
                    mCheckView.setEnabled(!mSelectedCollection.maxSelectableReached());
                }
            }
            updateSize(item);
        }
        mPreviousPos = position;
    }
}
