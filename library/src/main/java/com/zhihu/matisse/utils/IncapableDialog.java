package com.zhihu.matisse.utils;

import com.zhihu.matisse.ResourceTable;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class IncapableDialog extends CommonDialog {
    private final Context mContext;
    public IncapableDialog(Context context) {
        super(context);
        mContext = context;
    }

    public IncapableDialog setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            setTitleText(title);
        }
        return this;
    }

    public IncapableDialog setMessage(String message) {
        if (!TextUtils.isEmpty(message)) {
            setContentText(message);
        }
        return this;
    }
    public IncapableDialog setPositiveButton(){
        try {
            setButton(0, mContext.getResourceManager().getElement(ResourceTable.String_button_ok).getString(), (iDialog, i) -> iDialog.hide());
        } catch (IOException | NotExistException | WrongTypeException e) {
            L.d(e.getMessage());
        }
        return this;
    }
}
