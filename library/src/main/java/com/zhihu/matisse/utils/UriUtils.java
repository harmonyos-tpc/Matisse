/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zhihu.matisse.utils;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;

import ohos.utils.net.Uri;

import java.io.*;

/**
 * @author wallace
 */
public class UriUtils {

    private static final String SCHEME_DATA = "dataability";
    private static final String SCHEME_FILE = "file";
    private static final String SCHEME_HTTP = "http";
    private static final String SCHEME_HTTPS = "https";

    /**
     * uri to image
     * @param context context
     * @param uri Uri
     * @return PixelMap
     */
    public static ImageSource uriToImageSource(Context context, Uri uri){
        if (uri != null){
            ImageSource.SourceOptions options = new ImageSource.SourceOptions();
            if (isDataMediaUri(uri)) {//媒体文件
                return ImageSource.create(uriToInputStream(context, uri), options);
            } else if (isResourceUri(context, uri)) {//资源文件
                return ImageSource.create(uriToResource(context, uri), options);
            }  else if (isFileUri(uri)) {//文件
                return ImageSource.create(uriToFileInputStream(uri), options);
            }else if (isAssertUri(uri)) {//rawfile
                return ImageSource.create(uriRawFile(context,uri), options);
            } else if (isNetWorkUri(uri)) {//网络图片
                return null;
            }
        }
        return null;
    }

    /**
     * 是否是媒体文件
     * @param uri dataability:///media/***
     * @return is
     */
    public static boolean isDataMediaUri(Uri uri){
        if (uri.getDecodedPathList() != null){
            if (uri.getDecodedPathList().size() > 0){
                return uri.getScheme().equals(SCHEME_DATA) && uri.getEncodedAuthority().isEmpty() && uri.getDecodedPathList().get(0).equals("media");
            }
        }
        return false;
    }

    /**
     * 是否是rawfile文件
     * @param uri dataability:///resources/rawfile/...
     * @return is
     */
    public static boolean isAssertUri(Uri uri){
        if (uri.getDecodedPathList() != null){
            if (uri.getDecodedPathList().size() > 2){
                return uri.getScheme().equals(SCHEME_DATA)
                        && uri.getEncodedAuthority().isEmpty()
                        && uri.getDecodedPathList().get(0).equals("resources")
                        && uri.getDecodedPathList().get(1).equals("rawfile");
            }
        }
        return false;
    }

    /**
     * 是否是资源文件
     * @param uri dataability://bundleName/id
     * @param context context
     * @return is
     */
    public static boolean isResourceUri(Context context,Uri uri){
        return uri.getScheme().equals(SCHEME_DATA) && uri.getDecodedAuthority().equals(context.getBundleName());
    }

    /**
     * 是否是网络图片
     * @param uri http://www.***.com or https://www.***.com
     * @return is
     */
    public static boolean isNetWorkUri(Uri uri){
        return uri.getScheme().equals(SCHEME_HTTP) || uri.getScheme().equals(SCHEME_HTTPS);
    }
    /**
     * 是否是文件
     * @param uri file://...
     * @return is
     */
    public static boolean isFileUri(Uri uri){
        return uri.getScheme().equals(SCHEME_FILE);
    }

    private static FileInputStream uriToFileInputStream(Uri uri) {
        try {
            File file = new File(uri.getDecodedPath());
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            return null;
        }

    }

    private static FileDescriptor uriToInputStream(Context context, Uri uri) {
        try {
            return DataAbilityHelper.creator(context).openFile(uri, "r");
        } catch (DataAbilityRemoteException | FileNotFoundException e) {
            return null;
        }

    }

    private static Resource uriToResource(Context context, Uri uri) {
        try {
            return context.getResourceManager().getResource(Integer.parseInt(uri.getDecodedPath()));
        } catch (NotExistException | IOException e) {
            return null;
        }
    }

    private static Resource uriRawFile(Context context, Uri uri) {
        try {
            RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry("resources/rawfile/" + uri.getLastPath());
            return rawFileEntry.openRawFile();
        } catch (IOException e) {
            return null;
        }
    }
}
