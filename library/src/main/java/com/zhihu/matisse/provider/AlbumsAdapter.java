package com.zhihu.matisse.provider;

import com.zhihu.matisse.ResourceTable;
import com.zhihu.matisse.model.Album;
import com.zhihu.matisse.model.SelectionSpec;
import com.zhihu.matisse.utils.L;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

import java.util.ArrayList;

public class AlbumsAdapter extends BaseItemProvider {

    private ArrayList<Album> list;
    private Context mContext;
    private ShapeElement mPlaceholder;
    public AlbumsAdapter(Context context,ArrayList<Album> list){
        mContext = context;
        this.list = list;

        mPlaceholder = new ShapeElement();
        mPlaceholder.setRgbColor(RgbColor.fromArgbInt(context.getColor(ResourceTable.Color_zhihu_item_placeholder)));
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        return list == null ? null : list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return list == null ? 0 : list.get(i).hashCode();
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component view = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_album_list_item,componentContainer,false);

        Album album = (Album) getItem(i);
        ((Text) view.findComponentById(ResourceTable.Id_album_name)).setText(album.getDisplayName(mContext));
        ((Text) view.findComponentById(ResourceTable.Id_album_media_count)).setText(String.valueOf(album.getCount()));
        Image image = (Image) view.findComponentById(ResourceTable.Id_album_cover);
        SelectionSpec.getInstance().imageEngine.loadThumbnail(mContext, AttrHelper.fp2px(40,mContext),mPlaceholder,image,album.getCoverUri());
        return view;
    }
}
